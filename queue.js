let collection = [];
let rear = 0;

// Write the queue functions below.

function print () {
    return collection;
};

function enqueue (element) {
    collection[rear] = element;
    rear = rear + 1;
    return collection;
}

function dequeue () {
    rear = rear - 1;
    const [,...newArray] = collection;
    collection = newArray;
    return collection;
}

function front () {
    return collection[0];
}

function size () {
    return rear;
}
function isEmpty () {
    if(collection.length !== 0){
        return false;
    }
    else{
        return true;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};